/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo01;

import java.util.Scanner;

/**
 *
 * @author rodri
 */
public class POO01 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        
        Persona p = new Persona();
        Alumno a = new Alumno();
        
        try {
            
            p.setId(10);
            System.out.println("p.id: " + p.getId());
            
            System.out.println("Nombre del alumno: ");
            a.setNombre(s.next());
            
            System.out.println("Promedio: ");
            a.setPromedio(s.nextFloat());
            
            System.out.println("Nombre: " + a.getNombre());
            System.out.println("Promedio: " + a.getPromedio());
            
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }    
    }
    
}
