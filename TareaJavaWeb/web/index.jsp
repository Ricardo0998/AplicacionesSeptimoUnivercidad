<%@page contentType="text/html" pageEncoding="UTF-8"%>

<style>
    .centrado {
        position: center;
        left: 50%;
        top: 50%;
        transform: translate(-50%, -50%);
        -webkit-transform: translate(-50%, -50%);
    }
    .alineado{
        text-align: center;
    }
</style>

<!DOCTYPE html>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!--BOOTSTRAP-->
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body style="background-image: url('image/2018-ford-mustang-photos-and-info-news-car-and-driver-photo-674589-s-original.jpg')">
        <div class="card centrado col-sm-auto">
            <div class="card-header">
                Menu de opciones
            </div>
            <div class="card-body">
                <ul class="list-group">
                    <li class="list-group-item">
                        <a href="clientes.jsp">Insertar Clientes</a>
                    </li>
                    <li class="list-group-item">
                        <a href="proveedores.jsp">Insertar Proveedores</a>
                    </li>
                    <li class="list-group-item">
                        <a href="productos.jsp">Insertar Productos</a>
                    </li>
                </ul>
            </div> 
        </div>
            
        <!--Scripts-->
        <%@include file="scripts.html" %>
        
    </body>
</html>