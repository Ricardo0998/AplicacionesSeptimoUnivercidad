/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicaexamen;

/**
 *
 * @author rodri
 */
public class Esfera implements Figuras {
    private float radio;

    public Esfera(float radio) {
        this.radio = radio;
    }

    public float getRadio() {
        return radio;
    }

    public void setRadio(float radio) {
        this.radio = radio;
    }

    @Override
    public float Calcular_volumen() {
        return (float) (4 * Math.PI * Math.pow(radio,3)) / 3;
    }
    
}
