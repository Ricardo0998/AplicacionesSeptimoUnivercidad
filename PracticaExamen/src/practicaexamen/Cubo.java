
package practicaexamen;

/**
 *
 * @author rodri
 */
public class Cubo implements Figuras {
    private float lado;

    public Cubo(float lado) {
        this.lado = lado;
    }

    public float getLado() {
        return lado;
    }

    public void setLado(float lado) {
        this.lado = lado;
    }

    @Override
    public float Calcular_volumen() {
          return lado * lado * lado;
    }
    
}
