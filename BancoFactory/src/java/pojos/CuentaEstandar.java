/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojos;

/**
 *
 * @author rodri
 */
public class CuentaEstandar {
    private static double tasaInteres = 0.5;
    private static String regalo = "";
    
    private static int tDebito = 5;
    private static int tCredito = 0;
    private static int mtCredito = 0;
    
    private static int descuento = 0;

    public static double getTasaInteres() {
        return tasaInteres;
    }

    public static String getRegalo() {
        return regalo;
    }

    public static int gettDebito() {
        return tDebito;
    }

    public static int gettCredito() {
        return tCredito;
    }

    public static int getMtCredito() {
        return mtCredito;
    }

    public static int getDescuento() {
        return descuento;
    }
    
    
}
