<%-- 
    Document   : index
    Created on : 5/11/2018, 08:40:25 PM
    Author     : rodri
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script src="js/jquery.js"></script>
        
        <!--BOOTSTRAP-->
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div id="principal">
            <form action="" method="post"> 
                <label>Nombre</label>
                <input type="text" name="Nombre">
                <br>
                 <label>Edad</label>
                <input type="text" name="Edad">
                <br>
                 <label>Direccion</label>
                <input type="text" name="Direccion">
                <br>
                <label>¿Tiene pension?</label>
                <br>
                <input type="radio" name="Pension" value="PensionSi">Si
                <input type="radio" name="Pension" value="PensionNo">No
                <br>
                <label>Tarjeta de credito</label>
                <br>
                <input type="radio" name="TarjetaCredito" value="creditoSi">Si
                <input type="radio" name="TarjetaCredito" value="creditoNo">No
                <br>
                <label>Tarjeta de debito</label>
                <br>
                <input type="radio" name="TarjetaDebito" value="debitoSi">Si
                <input type="radio" name="TarjetaDebito" value="debitoNo">No
                <br>
                <input type="submit" value="Continuar">
            </form>
        </div>
    </body>
</html>
<script>
    function aparecer(){
        $('#principal').toggle();
        $('#Secundario').toggle();
    }
</script>